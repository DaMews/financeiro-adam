package br.com.financeiroAdam.demo;

import br.com.financeiroAdam.demo.dto.UsuarioSalvarDTO;
import br.com.financeiroAdam.demo.enums.TipoStatus;
import br.com.financeiroAdam.demo.mapper.UsuarioMapper;
import br.com.financeiroAdam.demo.model.Usuario;
import br.com.financeiroAdam.demo.service.UsuarioService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.transaction.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@Transactional
@AutoConfigureMockMvc
public class UsuarioControllerTests {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private Gson gson;

    @Test
    public void dadoUsuarioAtivo_quandoSalvo_entaoRetornaSucesso() throws Exception {

        //Dado
        Usuario usuario = new Usuario();
        usuario.setEmail("teste@test.com");
        usuario.setStatus(TipoStatus.ATIVO);

        ResultActions usuarioSalvo = mvc.perform(post("/financas/usuario")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(gson.toJson(usuario)));

        usuarioSalvo.andExpect(status().isCreated());
    }

    @Test
    public void dadoUsuarioInativo_quandoSalvo_entaoRetornaSucesso() throws Exception {

        //Dado
        Usuario usuario = new Usuario();
        usuario.setEmail("teste@test.com");
        usuario.setStatus(TipoStatus.INATIVO);

        ResultActions usuarioSalvo = mvc.perform(post("/financas/usuario")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(gson.toJson(usuario)));

        usuarioSalvo.andExpect(status().isCreated());
    }

    @Test
    public void dadoUsuarioInvalido_quandoSalvo_entaoNaoRetornaSucesso() throws Exception {

        //Dado
        Usuario usuario = new Usuario();

        //Quando
        ResultActions usuarioSalvo = mvc.perform(post("/financas/usuario")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(gson.toJson(usuario)));

        //Entao
        usuarioSalvo.andExpect(status().isBadRequest());
    }

    @Test
    public void dadoUsuario_quandoBusco_entaoRetornaUsuario() throws Exception {

        //Dado
        Usuario usuario = new Usuario();
        usuario.setEmail("test@test.com");

        String conteudoRetorno = mvc.perform(post("/financas/usuario")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content(gson.toJson(usuario)))
                .andReturn()
                .getResponse()
                .getContentAsString();

        Integer idUsuario = (Integer) new JSONObject(conteudoRetorno).get("id");


        //Quando
        String json = mvc.perform(get("/financas/usuario/:" + idUsuario)).andReturn().getResponse().getContentAsString();
        Usuario usuario1 = new GsonBuilder().create().fromJson(json, Usuario.class);

    }


}
