package br.com.financeiroAdam.demo;

import br.com.financeiroAdam.demo.dto.UsuarioSalvarDTO;
import br.com.financeiroAdam.demo.enums.TipoStatus;
import br.com.financeiroAdam.demo.mapper.UsuarioMapper;
import br.com.financeiroAdam.demo.model.Usuario;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class MapperTests {

    @Autowired
    private UsuarioMapper usuarioMapper;

    @Test
    public void dadoUsuarioSalvarDTO_quandoMapeioParaUsuario_entaoRetornaUsuario() {

        //Dado
        UsuarioSalvarDTO usuarioSalvarDTO = new UsuarioSalvarDTO();
        usuarioSalvarDTO.setNome("teste");
        usuarioSalvarDTO.setEmail("test@test.com");
        usuarioSalvarDTO.setStatus(TipoStatus.ATIVO);

        Usuario usuario = usuarioMapper.toEntity(usuarioSalvarDTO);

        Assert.assertEquals(usuario.getEmail(), "test@test.com");
        Assert.assertEquals(usuario.getNome(), "teste");
        Assert.assertEquals(usuario.getStatus(), TipoStatus.ATIVO);


    }
}
