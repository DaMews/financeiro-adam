package br.com.financeiroAdam.demo.enums;

public enum TipoStatus {
    INATIVO, ATIVO;
}
