package br.com.financeiroAdam.demo.enums;

public enum TipoBaixa {
    ENTRADA, SAIDA;
}
