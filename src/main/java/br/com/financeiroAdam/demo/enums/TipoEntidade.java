package br.com.financeiroAdam.demo.enums;

public enum TipoEntidade {
    CLIENTE, FORNECEDOR;
}
