package br.com.financeiroAdam.demo.enums;

public enum TipoStatusLancamentoFinanceiro {
    ABERTO, BAIXADO;
}
