package br.com.financeiroAdam.demo.controller;

import br.com.financeiroAdam.demo.exceptions.ContaNaoEncontradaException;
import br.com.financeiroAdam.demo.exceptions.LancamentoFinanceiroNaoEncontradoException;
import br.com.financeiroAdam.demo.service.BaixaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/financas/usuario/:idUsuario/conta/")
public class BaixaController {

    @Autowired
    private BaixaService baixaService;


    @ResponseStatus(HttpStatus.OK)
    @PostMapping(":{idConta}/:{idLancamento}")
    public ResponseEntity baixaLancamento(@RequestParam String acao,
                                          @PathVariable Long idConta,
                                          @PathVariable Long idLancamento) throws LancamentoFinanceiroNaoEncontradoException {
        baixaService.realizaBaixa(idConta, idLancamento, acao);
        return ResponseEntity.ok().body("Lançamento " + idLancamento + " com sucesso");
    }
}
