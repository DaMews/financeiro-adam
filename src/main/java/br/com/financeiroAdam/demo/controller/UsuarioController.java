package br.com.financeiroAdam.demo.controller;

import br.com.financeiroAdam.demo.dto.UsuarioSalvarDTO;
import br.com.financeiroAdam.demo.exceptions.UsuarioNaoEncontradoException;
import br.com.financeiroAdam.demo.service.UsuarioService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/financas/usuario")
@Api(description = "Controller dos usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @ApiOperation(value="Salva um usuario na plataforma.",
                notes="Caso não exista nenhum usuario com o id fornecido, um novo usuario será criado."
                        + "Caso contrário, os dados do usuario existente serão atualizados."
    )
    @ApiResponses(
            value = {
                    @ApiResponse(code = 201, message = "Usuario salvo com sucesso."),
                    @ApiResponse(code = 400, message = "Não foi possível processar esta operação.")
            }
    )
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public Object salvar(@ApiParam(value = "Objeto Usuário") @Valid @RequestBody UsuarioSalvarDTO usuarioSalvarDTO) throws Exception {
        return usuarioService.salvar(usuarioSalvarDTO);
    }

    @ApiOperation(value="Lista todos os usuarios cadastrados na plataforma.")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 201, message = "OK")
            }
    )
    @ResponseStatus(HttpStatus.OK)
    @GetMapping
    public ResponseEntity<Object> listar() {
        return ResponseEntity.ok(usuarioService.listar());
    }

    @ApiOperation(value="Busca um usuario pelo seu id.",
            notes="Caso não exista nenhum usuario com o id fornecido, uma mensagem com o erro será retornada."
                    + "Caso contrário, será retornado o usuario com o id passado."
    )
    @ApiResponses(
            value = {
                    @ApiResponse(code = 201, message = "Usuario encontrado."),
                    @ApiResponse(code = 404, message = "Usuario não encontrado.")
            }
    )
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/:{id}")
    public ResponseEntity<Object> buscar(@ApiParam(value = "Id do usuário") @PathVariable Long id) throws UsuarioNaoEncontradoException {
        return ResponseEntity.ok(usuarioService.buscar(id));
    }

}
