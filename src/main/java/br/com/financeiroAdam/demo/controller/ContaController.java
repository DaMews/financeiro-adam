package br.com.financeiroAdam.demo.controller;

import br.com.financeiroAdam.demo.dto.ContaSalvarDTO;
import br.com.financeiroAdam.demo.exceptions.ContaNaoEncontradaException;
import br.com.financeiroAdam.demo.service.ContaService;
import br.com.financeiroAdam.demo.service.LancamentoFinanceiroService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/financas/usuario")
@Api(description = "Controller de contas")
public class ContaController {

    @Autowired
    private ContaService contaService;

    @Autowired
    private LancamentoFinanceiroService lancamentoFinanceiroService;

    @ApiOperation(value = "Salva uma conta para um usuário na plataforma.",
            notes = "Será criada uma nova conta para o usuário com o id passado."
    )
    @ApiResponses(
            value = {
                    @ApiResponse(code = 201, message = "Conta salva com sucesso."),
                    @ApiResponse(code = 400, message = "Não foi possível processar esta operação.")
            }
    )
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(":{id}/conta")
    public ResponseEntity<Object> salvar(@ApiParam(value = "Nome da conta")
                                         @RequestBody ContaSalvarDTO contaSalvarDTO,
                                         @ApiParam(value = "Id do usuário")
                                            @PathVariable Long id) {
        return ResponseEntity.status(201).body(contaService.salvar(contaSalvarDTO, id));
    }

    @ApiOperation(value = "Lista todas as contas de um usuário com o id de usuário passado.")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 201, message = "OK")
            }
    )
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(":{idUsuario}/conta")
    public ResponseEntity<Object> listar(@ApiParam(value = "Id do usuário") @PathVariable Long idUsuario){
        return ResponseEntity.ok(contaService.listar(idUsuario));
    }

    @ApiOperation(value = "Busca uma conta pelo seu id.",
            notes = "Caso não exista nenhuma conta com o id fornecido, uma mensagem com o erro será retornada."
                    + "Caso contrário, será retornado a conta com o id passado."
    )
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "OK."),
                    @ApiResponse(code = 404, message = "Conta não encontrada.")
            }
    )
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(":id/conta/:{idConta}")
    public ResponseEntity<Object> buscar(@ApiParam(value = "Id da conta") @PathVariable Long idConta) throws ContaNaoEncontradaException {
        return ResponseEntity.ok(contaService.buscar(idConta));
    }


    @ApiOperation(value = "Lista todos os lançamentos financeiros de uma conta com o id de conta passado.")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "OK")
            }
    )
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(":id/conta/:{id}/lancamentos")
    public ResponseEntity<Object> listarLancamentos(@ApiParam(value = "Id da conta") @PathVariable Long id) {
        return ResponseEntity.ok(lancamentoFinanceiroService.listarPorConta(id));
    }

}
