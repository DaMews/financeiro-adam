package br.com.financeiroAdam.demo.controller;

import br.com.financeiroAdam.demo.dto.LancamentoFinanceiroSalvarDTO;
import br.com.financeiroAdam.demo.exceptions.LancamentoFinanceiroNaoEncontradoException;
import br.com.financeiroAdam.demo.service.LancamentoFinanceiroService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/financas/usuario")
@Api(description = "Controller dos Lancamentos Financeiros")
public class LancamentoFinanceiroController {

    @Autowired
    private LancamentoFinanceiroService lancamentoFinanceiroService;

    @ApiOperation(value="Salva um lançamento financeiro na plataforma.",
            notes="Caso não exista nenhum lançamento com o id fornecido, um novo lancamento será criado."
                    + "Caso contrário, os dados do lançamento existente serão atualizados."
    )
    @ApiResponses(
            value = {
                    @ApiResponse(code = 201, message = "Lançamento Financeiro salvo com sucesso."),
                    @ApiResponse(code = 400, message = "Não foi possível processar esta operação.")
            }
    )
    @ResponseStatus(HttpStatus.OK)
    @PostMapping(":{id}/entidade/:{idE}/lancamento")
    public ResponseEntity<Object> salvar(@RequestBody LancamentoFinanceiroSalvarDTO lancamento, @PathVariable(value = "id") Long id, @PathVariable(value = "idE") Long idE) {
        return ResponseEntity.status(201).body(lancamentoFinanceiroService.salvar(lancamento, id, idE));
    }

    @ApiOperation(value="Lista todos os lançamentos feitos por um usuário.")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 201, message = "OK")
            }
    )
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(":{id}/lancamentos")
    public ResponseEntity<Object> listarPorUsuario(@PathVariable Long id) {
        return ResponseEntity.ok(lancamentoFinanceiroService.listarPorUsuario(id));
    }

    @ApiOperation(value = "Lista todos os lançamentos feitos por uma conta.")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 201, message = "OK")
            }
    )
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(":idUsuario/conta/:{idConta}/lancamentos")
    public ResponseEntity<Object> listarPorConta(@PathVariable Long idConta) {
        return ResponseEntity.ok(lancamentoFinanceiroService.listarPorUsuario(idConta));
    }

    @ApiOperation(value="Busca um lançamento financeiro pelo seu id.",
            notes="Caso não exista nenhum lançamento com o id fornecido, uma mensagem com o erro será retornada."
                    + "Caso contrário, será retornado o lançamento com o id passado."
    )
    @ApiResponses(
            value = {
                    @ApiResponse(code = 200, message = "OK"),
                    @ApiResponse(code = 404, message = "Lançamento não encontrado.")
            }
    )
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(":idUsuario/lancamento/:{idLancamento}")
    public ResponseEntity<Object> buscar(@PathVariable Long idLancamento) throws LancamentoFinanceiroNaoEncontradoException {
        return ResponseEntity.ok(lancamentoFinanceiroService.buscar(idLancamento));
    }


}
