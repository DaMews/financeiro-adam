package br.com.financeiroAdam.demo.controller;

import br.com.financeiroAdam.demo.dto.EntidadeSalvarDTO;
import br.com.financeiroAdam.demo.exceptions.UsuarioNaoEncontradoException;
import br.com.financeiroAdam.demo.model.Entidade;
import br.com.financeiroAdam.demo.service.EntidadeService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/usuario")
@Api(description = "Controller das Entidades")
public class EntidadeController {

    @Autowired
    private EntidadeService entidadeService;

    @ApiOperation(value="Salva uma entidade para o usuario, com o id de usuário e a entidade fornecida na plataforma.",
            notes="Caso não exista nenhuma entidade com o id fornecido, uma nova entidade será criada."
                    + "Caso contrário, os dados da entidade existente serão atualizadas."
    )
    @ApiResponses(
            value = {
                    @ApiResponse(code = 201, message = "Entidade salva com sucesso."),
                    @ApiResponse(code = 400, message = "Não foi possível processar esta operação.")
            }
    )
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/:{id}/entidade")
    public ResponseEntity<Object> salvar(@ApiParam(value = "Objeto Entidade") @RequestBody EntidadeSalvarDTO entidadeSalvarDTO,
                                         @ApiParam(value = "Id do usuário") @PathVariable Long id){
        try {
            return ResponseEntity.status(201).body(entidadeService.salvar(entidadeSalvarDTO, id));
        } catch (Exception e){
            return ResponseEntity.status(401).body("Não foi possível processar esta operação." + e.getMessage());
        }
    }

    @ApiOperation(value="Lista todas as entidades de um usuario cadastradas na plataforma com base no id do usuário fornecido.")
    @ApiResponses(
            value = {
                    @ApiResponse(code = 201, message = "OK")
            }
    )
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/:{idUsuario}/entidade")
    public ResponseEntity<Object> listar(@ApiParam(value = "Id do usuário") @PathVariable Long idUsuario) throws UsuarioNaoEncontradoException {
        return ResponseEntity.ok(entidadeService.listar(idUsuario));
    }

    @ApiOperation(value="Busca uma entidade do usuario pelo seu id.",
            notes="Caso não exista nenhuma entidade com o id fornecido, uma mensagem com o erro será retornada."
                    + "Caso contrário, será retornada a entidade com o id passado."
    )
    @ApiResponses(
            value = {
                    @ApiResponse(code = 201, message = "OK"),
                    @ApiResponse(code = 400, message = "Entidade não encontrada.")
            }
    )
    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/:id/entidade:{id}")
    public ResponseEntity<Object> buscar(@ApiParam(value = "Id da entidade") @PathVariable Long id){
        try{
            return ResponseEntity.ok(entidadeService.buscar(id));
        } catch (Exception e) {
            return ResponseEntity.status(400).body(e.getMessage());
        }

    }




}
