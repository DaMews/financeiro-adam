package br.com.financeiroAdam.demo.service;

import br.com.financeiroAdam.demo.enums.TipoBaixa;
import br.com.financeiroAdam.demo.enums.TipoEntidade;
import br.com.financeiroAdam.demo.enums.TipoLancamentoFinanceiro;
import br.com.financeiroAdam.demo.enums.TipoStatusLancamentoFinanceiro;
import br.com.financeiroAdam.demo.exceptions.LancamentoFinanceiroNaoEncontradoException;
import br.com.financeiroAdam.demo.model.Baixa;
import br.com.financeiroAdam.demo.model.LancamentoFinanceiro;
import br.com.financeiroAdam.demo.repository.BaixaRepository;
import br.com.financeiroAdam.demo.repository.ContaRepository;
import br.com.financeiroAdam.demo.repository.LancamentoFinanceiroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BaixaService {

    @Autowired
    private LancamentoFinanceiroRepository lancamentoFinanceiroRepository;

    @Autowired
    private BaixaRepository baixaRepository;

    @Autowired
    private ContaRepository contaRepository;

    public void realizaBaixa(Long idConta, Long idLancamento, String acao) throws LancamentoFinanceiroNaoEncontradoException {

        if (!lancamentoFinanceiroRepository.existsById(idLancamento)) {
            throw new LancamentoFinanceiroNaoEncontradoException("Lançamento não encontrado!");
        }

        LancamentoFinanceiro l = lancamentoFinanceiroRepository.findById(idLancamento).get();
        Baixa baixa = new Baixa();
        baixa.setValor(l.getValor());
        baixa.setConta(contaRepository.findById(idConta).get());

        switch (acao) {
            case "baixar":
                l.setStatus(TipoStatusLancamentoFinanceiro.BAIXADO);
                if (l.getEntidade().getTipo() == TipoEntidade.CLIENTE) {
                    baixa.setTipo(TipoBaixa.ENTRADA);
                } else {
                    baixa.setTipo(TipoBaixa.SAIDA);
                }
                break;
            case "estornar":
                l.setStatus(TipoStatusLancamentoFinanceiro.ABERTO);
                if (l.getEntidade().getTipo() == TipoEntidade.CLIENTE) {
                    baixa.setTipo(TipoBaixa.ENTRADA);
                } else {
                    baixa.setTipo(TipoBaixa.SAIDA);
                }
                break;
        }
        l.setBaixa(baixa);
        baixaRepository.saveAndFlush(baixa);
        lancamentoFinanceiroRepository.saveAndFlush(l);
    }
}
