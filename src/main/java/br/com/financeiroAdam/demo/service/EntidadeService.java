package br.com.financeiroAdam.demo.service;

import br.com.financeiroAdam.demo.dto.EntidadeSalvaDTO;
import br.com.financeiroAdam.demo.dto.EntidadeSalvarDTO;
import br.com.financeiroAdam.demo.exceptions.EntidadeNaoEncontradaException;
import br.com.financeiroAdam.demo.mapper.EntidadeMapper;
import br.com.financeiroAdam.demo.model.Entidade;
import br.com.financeiroAdam.demo.model.Usuario;
import br.com.financeiroAdam.demo.repository.EntidadeRepository;
import br.com.financeiroAdam.demo.repository.UsuarioRepository;
import org.omg.IOP.ENCODING_CDR_ENCAPS;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class EntidadeService {

    @Autowired
    private EntidadeRepository entidadeRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private EntidadeMapper entidadeMapper;

    public EntidadeSalvaDTO salvar(EntidadeSalvarDTO entidadeSalvarDTO, Long idUsuario) {
        Usuario usuario = usuarioRepository.findById(idUsuario).get();
        Entidade entidade = entidadeRepository.saveAndFlush(entidadeMapper.toEntity(entidadeSalvarDTO, usuario));
        EntidadeSalvaDTO entidadeSalvaDTO = entidadeMapper.toDTO(entidade);
        entidadeSalvaDTO.setId_usuario(idUsuario);
        return entidadeSalvaDTO;
    }

    public List<EntidadeSalvaDTO> listar(Long id) {
        List<Entidade> entidades = entidadeRepository.findAll();
        List<EntidadeSalvaDTO> entidadeSalvaDTOS = new ArrayList<>();
        for(Entidade e: entidades){
            entidadeSalvaDTOS.add(entidadeMapper.toDTO(e));
        }
        return entidadeSalvaDTOS;
    }

    public EntidadeSalvaDTO buscar(Long id) throws EntidadeNaoEncontradaException{
        Optional<Entidade> entidade = entidadeRepository.findById(id);
        if(entidade.isPresent()){
            return entidadeMapper.toDTO(entidade.get());
        }
        throw new EntidadeNaoEncontradaException("Entidade não encontrada");
    }
}
