package br.com.financeiroAdam.demo.service;

import br.com.financeiroAdam.demo.dto.LancamentoFinanceiroSalvarDTO;
import br.com.financeiroAdam.demo.dto.LancamentoFinanceiroSalvoDTO;
import br.com.financeiroAdam.demo.enums.TipoBaixa;
import br.com.financeiroAdam.demo.enums.TipoLancamentoFinanceiro;
import br.com.financeiroAdam.demo.exceptions.LancamentoFinanceiroNaoEncontradoException;
import br.com.financeiroAdam.demo.exceptions.UsuarioNaoEncontradoException;
import br.com.financeiroAdam.demo.mapper.LancamentoFinanceiroMapper;
import br.com.financeiroAdam.demo.model.Baixa;
import br.com.financeiroAdam.demo.model.LancamentoFinanceiro;
import br.com.financeiroAdam.demo.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class LancamentoFinanceiroService {

    @Autowired
    private LancamentoFinanceiroRepository lancamentoFinanceiroRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private EntidadeRepository entidadeRepository;

    @Autowired
    private LancamentoFinanceiroMapper lancamentoFinanceiroMapper;

    public LancamentoFinanceiroSalvoDTO salvar(LancamentoFinanceiroSalvarDTO lancamentoFinanceiroSalvarDTO, Long id, Long idE) {
        LancamentoFinanceiro lancamentoFinanceiro = lancamentoFinanceiroMapper.toEntity(lancamentoFinanceiroSalvarDTO);
        lancamentoFinanceiro.setUsuario(usuarioRepository.findById(id).get());
        lancamentoFinanceiro.setEntidade(entidadeRepository.findById(idE).get());

        LancamentoFinanceiroSalvoDTO lancamentoFinanceiroSalvoDTO = lancamentoFinanceiroMapper
                .toDTO(lancamentoFinanceiroRepository.saveAndFlush(lancamentoFinanceiro));
        lancamentoFinanceiroSalvoDTO.setIdUsuario(id);

        return lancamentoFinanceiroSalvoDTO;
    }

    public List<LancamentoFinanceiroSalvoDTO> listarPorUsuario(Long id) {
        List<LancamentoFinanceiro> lancamentoFinanceiros = lancamentoFinanceiroRepository.findByUsuario_Id(id);
        List<LancamentoFinanceiroSalvoDTO> lancamentoFinanceiroSalvoDTOS = new ArrayList<>();
        for(LancamentoFinanceiro l: lancamentoFinanceiros){
            LancamentoFinanceiroSalvoDTO dto = lancamentoFinanceiroMapper.toDTO(l);
            dto.setIdUsuario(id);
            lancamentoFinanceiroSalvoDTOS.add(dto);
        }
        return lancamentoFinanceiroSalvoDTOS;
    }

    public List<LancamentoFinanceiroSalvoDTO> listarPorConta(Long id) {
        List<LancamentoFinanceiro> lancamentoFinanceiros = lancamentoFinanceiroRepository.findByConta_Id(id);
        List<LancamentoFinanceiroSalvoDTO> lancamentoFinanceiroSalvoDTOS = new ArrayList<>();
        for(LancamentoFinanceiro l: lancamentoFinanceiros){
            LancamentoFinanceiroSalvoDTO dto = lancamentoFinanceiroMapper.toDTO(l);
            dto.setIdUsuario(l.getUsuario().getId());
            lancamentoFinanceiroSalvoDTOS.add(dto);
        }
        return lancamentoFinanceiroSalvoDTOS;
    }

    public Object buscar(Long id) throws LancamentoFinanceiroNaoEncontradoException {
        Optional<LancamentoFinanceiro> lancamentoFinanceiro = lancamentoFinanceiroRepository.findById(id);

        if (lancamentoFinanceiro.isPresent()) {
            LancamentoFinanceiroSalvoDTO lancamentoFinanceiroSalvoDTO = lancamentoFinanceiroMapper.toDTO(lancamentoFinanceiro.get());
            lancamentoFinanceiroSalvoDTO.setIdUsuario(lancamentoFinanceiro.get().getUsuario().getId());
            return lancamentoFinanceiroSalvoDTO;
        }

        throw new LancamentoFinanceiroNaoEncontradoException("Lancamento não encontrado!");
    }

}
