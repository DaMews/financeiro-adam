package br.com.financeiroAdam.demo.service;

import br.com.financeiroAdam.demo.dto.ContaSalvaDTO;
import br.com.financeiroAdam.demo.dto.ContaSalvarDTO;
import br.com.financeiroAdam.demo.exceptions.ContaNaoEncontradaException;
import br.com.financeiroAdam.demo.mapper.ContaMapper;
import br.com.financeiroAdam.demo.model.Conta;
import br.com.financeiroAdam.demo.model.Usuario;
import br.com.financeiroAdam.demo.repository.ContaRepository;
import br.com.financeiroAdam.demo.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ContaService {

    @Autowired
    private ContaRepository contaRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private ContaMapper contaMapper;

    public ContaSalvaDTO salvar(ContaSalvarDTO contaSalvarDTO, Long id) {
        Usuario usuario = usuarioRepository.findById(id).get();
        Conta conta = contaMapper.toEntity(contaSalvarDTO, usuario);
        return contaMapper.toDTO(contaRepository.saveAndFlush(conta));
    }

    public List<ContaSalvaDTO> listar(Long id) {
        List<Conta> contas = contaRepository.findByUsuarioId(id);
        List<ContaSalvaDTO> contaSalvaDTOS = new ArrayList<>();
        for(Conta c: contas){
            contaSalvaDTOS.add(contaMapper.toDTO(c));
        }
        return contaSalvaDTOS;
    }

    public Object buscar(Long id) throws ContaNaoEncontradaException{
        Optional<Conta> conta = contaRepository.findById(id);

        if(conta.isPresent()){
            return contaMapper.toDTO(conta.get());
        }

        throw new ContaNaoEncontradaException("Conta não encontrada!");
    }
}
