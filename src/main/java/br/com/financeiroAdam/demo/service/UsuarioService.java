package br.com.financeiroAdam.demo.service;

import br.com.financeiroAdam.demo.dto.UsuarioComSaldoDTO;
import br.com.financeiroAdam.demo.dto.UsuarioSalvarDTO;
import br.com.financeiroAdam.demo.dto.UsuarioSalvoDTO;
import br.com.financeiroAdam.demo.exceptions.UsuarioNaoEncontradoException;
import br.com.financeiroAdam.demo.mapper.UsuarioMapper;
import br.com.financeiroAdam.demo.model.Saldo;
import br.com.financeiroAdam.demo.model.Usuario;
import br.com.financeiroAdam.demo.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private UsuarioMapper usuarioMapper;

    @Autowired
    private Saldo saldo;

    public UsuarioSalvoDTO salvar(UsuarioSalvarDTO usuarioSalvarDTO) throws Exception {
        Usuario usuario = usuarioMapper.toEntity(usuarioSalvarDTO);
        return usuarioMapper.toDTO(usuarioRepository.saveAndFlush(usuario));
    }

    public List<UsuarioSalvoDTO> listar() {
        List<Usuario> usuarios = usuarioRepository.findAll();
        List<UsuarioSalvoDTO> usuarioSalvoDTOS = new ArrayList<>();
        for (Usuario u : usuarios) {
            usuarioSalvoDTOS.add(usuarioMapper.toDTO(u));
        }
        return usuarioSalvoDTOS;
    }

    public UsuarioComSaldoDTO buscar(Long id) throws UsuarioNaoEncontradoException {
        Optional<Usuario> usuario = usuarioRepository.findById(id);
        Saldo saldo = new Saldo();

        if (usuario.isPresent()) {
            return usuarioMapper.toDTOSaldo(usuario.get(), saldo);
        }

        throw new UsuarioNaoEncontradoException("Usuario não encontrado!");
    }


}
