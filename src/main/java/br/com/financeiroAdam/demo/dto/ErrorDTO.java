package br.com.financeiroAdam.demo.dto;

import lombok.Builder;
import lombok.Data;

import java.util.Map;

@Builder
@Data
public class ErrorDTO {

    private int status;
    private String message;
}
