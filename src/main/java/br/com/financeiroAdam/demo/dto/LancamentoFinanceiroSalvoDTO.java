package br.com.financeiroAdam.demo.dto;

import br.com.financeiroAdam.demo.enums.TipoLancamentoFinanceiro;
import br.com.financeiroAdam.demo.enums.TipoStatusLancamentoFinanceiro;
import br.com.financeiroAdam.demo.model.Baixa;
import br.com.financeiroAdam.demo.model.Usuario;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LancamentoFinanceiroSalvoDTO {

    private Long idUsuario;

    private Long id;

    private String descricao;

    private String observacao;

    private BigDecimal valor;

    private TipoLancamentoFinanceiro tipo;

    private Baixa baixa;

    private TipoStatusLancamentoFinanceiro status;

    private Date createdAt;

}
