package br.com.financeiroAdam.demo.dto;

import br.com.financeiroAdam.demo.enums.TipoStatus;
import br.com.financeiroAdam.demo.model.Saldo;
import lombok.Data;

@Data
public class UsuarioComSaldoDTO {

    private Long id;

    private String nome;

    private String email;

    private Saldo saldo;

    private TipoStatus status;

}
