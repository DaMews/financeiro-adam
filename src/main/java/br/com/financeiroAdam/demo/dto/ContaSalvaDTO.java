package br.com.financeiroAdam.demo.dto;

import br.com.financeiroAdam.demo.enums.TipoStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ContaSalvaDTO {

    private Long id;

    private BigDecimal saldo;

    private TipoStatus tipo;

    private Date createdAt;
}
