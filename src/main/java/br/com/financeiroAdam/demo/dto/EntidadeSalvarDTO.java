package br.com.financeiroAdam.demo.dto;

import br.com.financeiroAdam.demo.enums.TipoEntidade;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EntidadeSalvarDTO {

    private String nome;

    private TipoEntidade tipo;

}
