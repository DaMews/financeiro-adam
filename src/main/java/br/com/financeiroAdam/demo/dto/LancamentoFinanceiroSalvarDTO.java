package br.com.financeiroAdam.demo.dto;

import br.com.financeiroAdam.demo.enums.TipoLancamentoFinanceiro;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LancamentoFinanceiroSalvarDTO {
    String descricao;

    String observacao;

    BigDecimal valor;

    TipoLancamentoFinanceiro tipo;
}
