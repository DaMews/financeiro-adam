package br.com.financeiroAdam.demo.dto;

import br.com.financeiroAdam.demo.enums.TipoStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioSalvoDTO {

    Long id;

    String nome;

    String email;

    TipoStatus status;

    Date createdAt;
}
