package br.com.financeiroAdam.demo.dto;

import br.com.financeiroAdam.demo.enums.TipoStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioSalvarDTO {

    private String nome;

    private String email;

    private TipoStatus status;
}
