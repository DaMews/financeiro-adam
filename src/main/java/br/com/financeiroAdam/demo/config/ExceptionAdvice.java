package br.com.financeiroAdam.demo.config;

import br.com.financeiroAdam.demo.dto.ErrorDTO;
import br.com.financeiroAdam.demo.exceptions.ContaNaoEncontradaException;
import br.com.financeiroAdam.demo.exceptions.EntidadeNaoEncontradaException;
import br.com.financeiroAdam.demo.exceptions.LancamentoFinanceiroNaoEncontradoException;
import br.com.financeiroAdam.demo.exceptions.UsuarioNaoEncontradoException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ExceptionAdvice {

    @ExceptionHandler({UsuarioNaoEncontradoException.class
            , ContaNaoEncontradaException.class
            , EntidadeNaoEncontradaException.class
            , LancamentoFinanceiroNaoEncontradoException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ErrorDTO handleNaoEncontrados(UsuarioNaoEncontradoException ex) {
        return ErrorDTO.builder()
                .message(ex.getMessage())
                .status(HttpStatus.NOT_FOUND.value())
                .build();
    }


    @ExceptionHandler(Throwable.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ErrorDTO handleAllErros(Throwable t) {
        return ErrorDTO.builder()
                .message("Não foi possível processar esta operação")
                .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .build();
    }
}
