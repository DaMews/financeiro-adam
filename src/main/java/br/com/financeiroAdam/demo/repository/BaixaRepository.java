package br.com.financeiroAdam.demo.repository;

import br.com.financeiroAdam.demo.model.Baixa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BaixaRepository extends JpaRepository<Baixa, Long> {
}
