package br.com.financeiroAdam.demo.repository;

import br.com.financeiroAdam.demo.model.Entidade;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EntidadeRepository extends JpaRepository<Entidade, Long> {

    List<Entidade> findByUsuarioId(Long id);
}
