package br.com.financeiroAdam.demo.repository;

import br.com.financeiroAdam.demo.model.LancamentoFinanceiro;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface LancamentoFinanceiroRepository extends JpaRepository<LancamentoFinanceiro, Long> {

    List<LancamentoFinanceiro> findByUsuario_Id(Long id);

    @Query("select l from LancamentoFinanceiro l where l.usuario in (select c.usuario from Conta c where c.id = ?1)")
    List<LancamentoFinanceiro> findByConta_Id(Long idL);

}
