package br.com.financeiroAdam.demo.repository;

import br.com.financeiroAdam.demo.model.Baixa;
import br.com.financeiroAdam.demo.model.Usuario;
import org.hibernate.sql.Select;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

}
