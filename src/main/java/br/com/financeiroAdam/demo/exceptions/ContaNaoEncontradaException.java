package br.com.financeiroAdam.demo.exceptions;

public class ContaNaoEncontradaException extends Exception {
    public ContaNaoEncontradaException(String s) {
        super(s);
    }
}
