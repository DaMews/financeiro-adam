package br.com.financeiroAdam.demo.exceptions;

public class UsuarioNaoEncontradoException extends Exception {

    public UsuarioNaoEncontradoException(String s) {
        super(s);
    }
}
