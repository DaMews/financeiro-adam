package br.com.financeiroAdam.demo.exceptions;

public class LancamentoFinanceiroNaoEncontradoException extends Exception {
    public LancamentoFinanceiroNaoEncontradoException(String s) {
        super(s);
    }
}
