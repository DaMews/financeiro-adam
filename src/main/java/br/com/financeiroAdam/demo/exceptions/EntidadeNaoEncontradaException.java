package br.com.financeiroAdam.demo.exceptions;

public class EntidadeNaoEncontradaException extends Exception {
    public EntidadeNaoEncontradaException(String s) {
        super(s);
    }
}
