package br.com.financeiroAdam.demo.model;

import br.com.financeiroAdam.demo.enums.TipoLancamentoFinanceiro;
import br.com.financeiroAdam.demo.enums.TipoStatusLancamentoFinanceiro;
import lombok.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@AllArgsConstructor
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@Builder
public class LancamentoFinanceiro extends Auditavel<Long> {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private String descricao;

    @Column
    private String observacao;

    @NotNull
    private BigDecimal valor;

    @Column
    private Date dataBaixa;

    @Builder.Default
    @Enumerated(EnumType.STRING)
    @NotNull
    private TipoStatusLancamentoFinanceiro status = TipoStatusLancamentoFinanceiro.ABERTO;

    @Enumerated(EnumType.STRING)
    @NotNull
    private TipoLancamentoFinanceiro tipo;

    @ManyToOne(optional = false)
    private Usuario usuario;

    @ManyToOne(optional = false)
    private Entidade entidade;

    @OneToOne
    private Baixa baixa;
}
