package br.com.financeiroAdam.demo.model;

import br.com.financeiroAdam.demo.enums.TipoBaixa;
import lombok.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@EqualsAndHashCode(callSuper=false)
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@Builder
public class Baixa extends Auditavel<Long> {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private BigDecimal valor;

    @Enumerated(EnumType.STRING)
    @NotNull
    private TipoBaixa tipo;

    @ManyToOne(optional = false)
    private Conta conta;
}
