package br.com.financeiroAdam.demo.model;

import br.com.financeiroAdam.demo.enums.TipoEntidade;
import br.com.financeiroAdam.demo.enums.TipoStatus;
import lombok.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper=false)
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@Builder
public class Entidade extends Auditavel<Long> {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private String nome;

    @Enumerated(EnumType.STRING)
    @NotNull
    private TipoEntidade tipo;

    @Enumerated(EnumType.STRING)
    @NotNull
    @Builder.Default
    private TipoStatus status = TipoStatus.ATIVO;

    @ManyToOne(optional = false)
    private Usuario usuario;

}
