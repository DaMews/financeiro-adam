package br.com.financeiroAdam.demo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import java.math.BigDecimal;

@Component
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Saldo {

    @Builder.Default
    private BigDecimal total = new BigDecimal(0);

    @Builder.Default
    private BigDecimal receber = new BigDecimal(0);

    @Builder.Default
    private BigDecimal pagar = new BigDecimal(0);

}
