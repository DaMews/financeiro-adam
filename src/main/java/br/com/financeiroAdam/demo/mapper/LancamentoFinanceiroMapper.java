package br.com.financeiroAdam.demo.mapper;

import br.com.financeiroAdam.demo.dto.LancamentoFinanceiroSalvarDTO;
import br.com.financeiroAdam.demo.dto.LancamentoFinanceiroSalvoDTO;
import br.com.financeiroAdam.demo.model.LancamentoFinanceiro;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface LancamentoFinanceiroMapper {

    @Mappings({
            @Mapping(target = "id" , ignore = true),
            @Mapping(target = "dataBaixa" , ignore = true),
            @Mapping(target = "status" , ignore = true),
            @Mapping(target = "usuario" , ignore = true),
            @Mapping(target = "entidade" , ignore = true),
            @Mapping(target = "baixa" , ignore = true),
            @Mapping(target = "createdAt", ignore = true),
            @Mapping(target = "updatedAt", ignore = true)
    })
    LancamentoFinanceiro toEntity(LancamentoFinanceiroSalvarDTO lancamentoFinanceiroSalvarDTO);

    @Mappings({
            @Mapping(target = "idUsuario", ignore = true)
    })
    LancamentoFinanceiroSalvoDTO toDTO(LancamentoFinanceiro lancamentoFinanceiro);

}
