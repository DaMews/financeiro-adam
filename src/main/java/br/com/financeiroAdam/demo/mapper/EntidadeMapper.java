package br.com.financeiroAdam.demo.mapper;

import br.com.financeiroAdam.demo.dto.EntidadeSalvaDTO;
import br.com.financeiroAdam.demo.dto.EntidadeSalvarDTO;
import br.com.financeiroAdam.demo.model.Entidade;
import br.com.financeiroAdam.demo.model.Usuario;
import org.mapstruct.*;

@Mapper(componentModel = "spring")
public interface EntidadeMapper {

    @Mappings({
            @Mapping(source = "entidadeSalvarDTO.nome", target = "nome"),
            @Mapping(source = "usuarioEntity", target = "usuario"),
            @Mapping(target = "createdAt", ignore = true),
            @Mapping(target = "updatedAt", ignore = true),
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "status", ignore = true)

    })
    Entidade toEntity(EntidadeSalvarDTO entidadeSalvarDTO, Usuario usuarioEntity);

    @Mapping(source="entidade.usuario.id", target = "id_usuario")
    EntidadeSalvaDTO toDTO(Entidade entidade);



}
