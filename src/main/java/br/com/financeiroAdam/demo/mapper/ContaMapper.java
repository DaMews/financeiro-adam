package br.com.financeiroAdam.demo.mapper;

import br.com.financeiroAdam.demo.dto.ContaSalvaDTO;
import br.com.financeiroAdam.demo.dto.ContaSalvarDTO;
import br.com.financeiroAdam.demo.model.Conta;
import br.com.financeiroAdam.demo.model.Usuario;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.math.BigDecimal;

@Mapper(componentModel = "spring")
public interface ContaMapper {

    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "tipo", ignore = true),
            @Mapping(target = "createdAt", ignore = true),
            @Mapping(target = "updatedAt", ignore = true),
            @Mapping(source = "usuarioEntity", target = "usuario"),
            @Mapping(source = "contaSalvarDTO.nome", target = "nome")
    })
    Conta toEntity(ContaSalvarDTO contaSalvarDTO, Usuario usuarioEntity);

    @Mappings({
            @Mapping(source = "conta.tipo", target = "tipo")
    })
    ContaSalvaDTO toDTO(Conta conta);
}
