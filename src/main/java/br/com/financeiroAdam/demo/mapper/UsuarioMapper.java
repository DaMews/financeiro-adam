package br.com.financeiroAdam.demo.mapper;

import br.com.financeiroAdam.demo.dto.UsuarioComSaldoDTO;
import br.com.financeiroAdam.demo.dto.UsuarioSalvarDTO;
import br.com.financeiroAdam.demo.dto.UsuarioSalvoDTO;
import br.com.financeiroAdam.demo.model.Saldo;
import br.com.financeiroAdam.demo.model.Usuario;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import javax.persistence.MapsId;

@Mapper(componentModel="spring")
public interface UsuarioMapper {

    @Mappings({
            @Mapping(target = "id", ignore = true),
            @Mapping(target = "createdAt", ignore = true),
            @Mapping(target = "updatedAt", ignore = true),
            @Mapping(source="usuarioSalvarDTO.email", target = "email")
    })
    Usuario toEntity(UsuarioSalvarDTO usuarioSalvarDTO);

    UsuarioSalvoDTO toDTO(Usuario usuario);

    UsuarioComSaldoDTO toDTOSaldo(Usuario usuario, Saldo saldo);

//    public abstract UsuarioComSaldoDTO toDTOSaldo(Usuario usuario);


}
